# BaseProject PaymentIntegration

- The base project includes various types of payment integrations in iOS. The necessary code has been completed in this base setup. you only need to use those functions in your project.

- The following payment methods are set up in this project.
 
#### [• Stripe](https://gitlab.com/snehgtridhyatech/baseproject_paymentintegration#stripe)

#### [• RazorPay](https://gitlab.com/snehgtridhyatech/baseproject_paymentintegration#razorpay)

#### [• PayPal](https://gitlab.com/snehgtridhyatech/baseproject_paymentintegration#paypal)



## Prerequisites

1. Add following code in your **info.plist**.
```
<key>UIBackgroundModes</key>
<array>
    <string>fetch</string>
    <string>processing</string>
</array>
```



## Getting started

1. Clone the project from here:

```
git clone https://gitlab.com/snehgtridhyatech/baseproject_paymentintegration.git
```

2. Copy required files from **Manager** folder from this project into your project.



## Razorpay

### Requirements

- Create Razorpay account and generate keys from [here](https://razorpay.com)

### Dependancies

1. Add following pod into your **Podfile**.

```
pod 'razorpay-pod'
```

2. Add following code in your info.plist file.

```
<key>LSApplicationQueriesSchemes</key>
<array>
    <string>tez</string>
    <string>phonepe</string>
    <string>paytmmp</string>
    <string>googlepay</string></array>
```

### Usage

1. Add following code to create **Razorpay** payment.

```
let model = RazorpayPaymentModel(amount: "{YOUR_AMOUNT}")
            
RazorpayManager.shared.setup(KeyId: "{YOUR_RAZORPAY_KEYID}",
                                         SecretKey: "{YOUR_RAZORPAY_SECRETKEY}",
                                         Parent: {PARENT_CONTROLLER},
                                         LiveMode: false,
                                         RazorpayPaymentModel: model)
{ isSuccess, response in
                
    print("isSuccess: ",isSuccess,"\n","response: ",response)
}
            
```
2. Enter your amount at "{YOUR_AMOUNT}". 
3. Enter your razorpay key id at "{YOUR_RAZORPAY_KEYID}".
4. Enter your razorpay secret key id at "{YOUR_RAZORPAY_SECRETKEY}".
5. Enter controller from where you are using this function at "{PARENT_CONTROLLER}".
6. Set LiveMode as "true" for Production mode and "false" for Debug mode.

Your basic razor pay payment is completed from just above code.
You can modify image,name and other things. for that pass values in RazorpayPaymentModel()

### Screenshots

![image info](Screenshots/home.png){:height="400px" width="200px" padding-right:10px}
![image info](Screenshots/razorpay.png){:height="400px" width="200px" padding-right:10px}
![image info](Screenshots/razorpaySuccess.png){:height="400px" width="200px"}



## Paypal

### Requirements

- Login to Paypal developer account and generate sandbox account from [here](https://developer.paypal.com/dashboard/)
- Login to Braintree developer account from [here](https://sandbox.braintreegateway.com)
- Go to settings -> API -> Generate tokenization key 

### Dependancies

1. Add following pod into your **Podfile**.

```
pod 'Braintree'
```

### Usage

1. Add following code in your info.plist file.

```
<key>CFBundleURLTypes</key>
<array>
<dict>
        <key>CFBundleTypeRole</key>
        <string>Editor</string>
        <key>CFBundleURLName</key>
        <string>{YOUR_BUNDLE_IDENTIFIER}</string>
        <key>CFBundleURLSchemes</key>
        <array>
            <string>{YOUR_BUNDLE_IDENTIFIER}.payments</string>
        </array>
    </dict>
</array>
```

2. Add following code into your **Appdelgate.swift**.


```
import Braintree
```

```
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
    BTAppContextSwitcher.sharedInstance.returnURLScheme = "{YOUR_BUNDLE_INDENTIFIER}.payments"
        
    return true
}
    
func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
    if url.scheme?.localizedCaseInsensitiveCompare("{YOUR_BUNDLE_INDENTIFIER}.payments") == .orderedSame {
        return BTAppContextSwitcher.sharedInstance.handleOpen(url)
    }
        
    return false
}
```

3. Add following code to create **Paypal** payment.

```
PaypalManager.shared.getAccountNonce(Authkey: "{YOUR_PAYPAL_AUTHKEY}")
{ isSuccess, response in
            
    print("isSuccess: ",isSuccess,"\n","response: ",response)
}
            
```

4. Enter your generated paypal tokenization key at "{YOUR_PAYPAL_AUTHKEY}".
5. When this function calls, you will be redirect to paypal account page.
6. Use your generated sandox account credentials for login.
7. After successfull response pass you will receive token or nonce from response.

You can get paypal nonce by using just above function. Then pass that nonce to your backend team and do further prcesses.

### Screenshots

![image info](Screenshots/home.png){:height="400px" width="200px" padding-right:10px}
![image info](Screenshots/paypal.png){:height="400px" width="200px" padding-right:10px}



## Stripe

### Requirements

- Follow the document and generate Stripe keys from [here](https://stripe.com/docs/keys)

### Dependancies

1. Add following pod into your **Podfile**.

```
pod 'Stripe'
```

### Usage

1. Add following code to create **Stripe** payment.

```
let cardModel = StripeCardModel(cardNumber: {YOUR_CARD_NUMBER}, 
                                        cardExpMonth: {YOUR_CARD_EXP_MONTH},
                                        cardExpYear: {YOUR_CARD_EXP_YEAR},
                                        cardCVV: {YOUR_CARD_CVV})
        
let paymentModel = StripePaymentModel(amount: {YOUR_AMOUNT}, 
                                              currency: {YOUR_CURRENCY},
                                              paymentDesc: {YOUR_PAYMENT_DESC})
        
StripeManager.shared.setup(StripeCardModel: cardModel,
                                   StripePaymentModel: paymentModel,
                                   PublishKey: {YOUR_STRIPE_PUBLISH_KEY},
                                   AuthKey: {YOUR_STRIPE_AUTH_KEY}, 
                                   LiveMode: false)
{ isSuccess, response in
            
    print("isSuccess: ",isSuccess,"\n","response: ",response)
}
            
```
2. Enter your amount at "{YOUR_AMOUNT}".
2. Enter your card number at "{YOUR_CARD_NUMBER}".
2. Enter your card expiry month at "{YOUR_CARD_EXP_MONTH}".
2. Enter your card expiry year at "{YOUR_CARD_EXP_YEAR}".
2. Enter your card cvv at "{YOUR_CARD_CVV}".
2. Enter your amount at "{YOUR_AMOUNT}".
2. Enter your currency at "{YOUR_CURRENCY}".
2. Enter your payment description at "{YOUR_PAYMENT_DESC}".
3. Enter your stripe publish key at "{YOUR_STRIPE_PUBLISH_KEY}".
3. Enter your stripe auth key at "{YOUR_STRIPE_AUTH_KEY}".
5. Set LiveMode as "true" for Production mode and "false" for Debug mode.

### Screenshots

![image info](Screenshots/home.png){:height="400px" width="200px" padding-right:10px}
![image info](Screenshots/stripe.png){:height="400px" width="200px" padding-right:10px}
![image info](Screenshots/stripeSuccess.png){:height="400px" width="200px" padding-right:10px}


