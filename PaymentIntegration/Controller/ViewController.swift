//
//  ViewController.swift
//  PaymentIntegration
//
//  Created by Sneh on 10/6/23.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var razorpayView: UIView!
    @IBOutlet weak var paypalView: UIView!
    @IBOutlet weak var stripeView: UIView!
    
    // MARK: - Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }
    
    // MARK: - Functions
    func setUp() {
        self.stripeView.addBorderAndShadow()
        self.paypalView.addBorderAndShadow()
        self.razorpayView.addBorderAndShadow()
    }
    
    func checkAmount() -> Bool {
        if self.txtAmount.text?.trimmingCharacters(in: .whitespaces) != "" {
            return true
        } else {
            self.showAlert(title: "Empty", message: "Please enter amount")
            return false
        }
    }
    
    // MARK: - Stripe
    @IBAction func btnStripeClicked(_ sender: UIButton) {
        if self.checkAmount() {
            let controller = self.storyboard?.instantiateViewController(identifier: "StripeController") as! StripeController
            controller.amount = self.txtAmount.text ?? ""
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    // MARK: - PayPal
    @IBAction func btnPaypalClicked(_ sender: UIButton) {
        if self.checkAmount() {
            
            PaypalManager.shared.getAccountNonce(Authkey: "sandbox_s98trb2n_f9ww89j2c3tvqnp2")
            { isSuccess, response in
                if isSuccess {
                    print("TOKEN: - \(response)")
                    self.showAlert(title: "Success", message: "Token generated")
                } else {
                    self.showAlert(title: "Something went wrong", message: response)
                }
            }
        }
    }
    
    // MARK: - Razorpay
    @IBAction func btnRazorpayClicked(_ sender: UIButton) {
        if self.checkAmount() {
            
            let model = RazorpayPaymentModel(amount: self.txtAmount.text ?? "")
            
            RazorpayManager.shared.setup(KeyId: "rzp_test_JZHvGCKR4MXjYh",
                                         SecretKey: "vKuKZb7VwB5qWCXZIBQrLjpq",
                                         Parent: self,
                                         LiveMode: false,
                                         RazorpayPaymentModel: model)
            { isSuccess, response in
                if isSuccess {
                    self.showAlert(title: "Success", message: "Payment successfull")
                } else {
                    self.showAlert(title: "Error", message: response as! String)
                }
            }
        }
    }
    
}

// MARK: - Extension
extension UIViewController {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIView {
    func addBorderAndShadow() {
        self.layer.cornerRadius = 20
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 4
    }
}
