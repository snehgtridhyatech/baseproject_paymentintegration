//
//  ViewController.swift
//  PaymentIntegration
//
//  Created by Sneh on 10/5/23.
//

import UIKit
import Stripe

let stripePublishKey = "pk_test_51Ny713DJwctMlXw02mUqVbb18jMSsJIbLUdpHRlXgXuDYFINaTTmmImLayT8AgGtsLLs9kE6ZWjxX5J33Db4BtEB00HejzfB8P"
let stripeAuthKey = "sk_test_51Ny713DJwctMlXw0y8VljNxexig0ksHZJ4z2gVUDtpm01kQzgnQ0v4oNzhDVmtKJzzx6PrGbGLfeuy815QNC1AvR00MHGI9Iu8"


class StripeController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnDefaultCard: UIButton!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var defaultCardView: UIView!
    @IBOutlet weak var txtCardNumber: UITextField!
    
    // MARK: - Properties
    let cardTextField = STPPaymentCardTextField()
    var payWithDefaultCard = false
    var amount = ""
    
    // MARK: - Lifecylce
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    }
    
    // MARK: - Functions
    func setUp() {
        self.txtCardNumber.delegate = self
        self.txtCvv.delegate = self
        self.txtYear.delegate = self
        self.btnPay.layer.cornerRadius = 10
        self.btnDefaultCard.layer.cornerRadius = 10
        DispatchQueue.main.async {
            self.cardTextField.frame = self.defaultCardView.bounds
            self.defaultCardView.addSubview(self.cardTextField)
        }
    }
    
    func generateStripeToken() {
        let cardParams = STPCardParams()
        cardParams.number = self.payWithDefaultCard ? cardTextField.cardNumber : txtCardNumber.text!
        cardParams.expMonth = self.payWithDefaultCard ? UInt(cardTextField.expirationMonth) : UInt(txtYear.text!.prefix(2)) ?? 0
        cardParams.expYear = self.payWithDefaultCard ? UInt(cardTextField.expirationYear) : UInt(txtYear.text!.suffix(2)) ?? 0
        cardParams.cvc = self.payWithDefaultCard ? cardTextField.cvc : txtCvv.text!
        
        let cardModel = StripeCardModel(cardNumber: cardParams.number ?? "", 
                                        cardExpMonth: cardParams.expMonth,
                                        cardExpYear: cardParams.expYear,
                                        cardCVV: cardParams.cvc ?? "")
        
        let paymentModel = StripePaymentModel(amount: self.amount, 
                                              currency: .INR,
                                              paymentDesc: "Test")
        
        // MARK: - Stripe Manager Class
        StripeManager.shared.setup(StripeCardModel: cardModel,
                                   StripePaymentModel: paymentModel,
                                   PublishKey: stripePublishKey,
                                   AuthKey: stripeAuthKey, 
                                   LiveMode: true)
        { isSuccess, response in
            if isSuccess {
                self.showAlert(title: "Success", message: "Payment success")
            } else {
                self.showAlert(title: "Something went wrong", message: response)
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func btnPayClicked(_ sender: UIButton) {
        self.payWithDefaultCard = false
        if txtCardNumber.text != "" && txtYear.text != "" && txtCvv.text != "" {
            self.generateStripeToken()
        } else {
            self.showAlert(title: "Empty", message: "Please enter required details")
        }
    }
    
    @IBAction func btnDefaultCardClicked(_ sender: UIButton) {
        self.payWithDefaultCard = true
        self.generateStripeToken()
    }
    
}


// MARK: - Extension
extension StripeController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case txtCardNumber:
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        case txtYear:
            if textField == txtYear{
              guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
              }
              let updatedText = oldText.replacingCharacters(in: r, with: string)
              if string == "" {
                if updatedText.count == 2 {
                  textField.text = "\(updatedText.prefix(1))"
                  return false
                }
              } else if updatedText.count == 1 {
                if updatedText > "1" {
                  textField.text = "0\(updatedText)/"
                  return false
                }
              } else if updatedText.count == 2 {
                if updatedText <= "12" { //Prevent user to not enter month more than 12
                  textField.text = "\(updatedText)/" //This will add “/” when user enters 2nd digit of month
                }
                return false
              } else if updatedText.count > 5 {
                return false
              }
            }
            return true
        
        case txtCvv:
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        default:
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }

    }
}
