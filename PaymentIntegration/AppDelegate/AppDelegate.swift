//
//  AppDelegate.swift
//  PaymentIntegration
//
//  Created by Sneh on 10/5/23.
//

import UIKit
import Braintree
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        BTAppContextSwitcher.sharedInstance.returnURLScheme = "com.tridhya.PaymentIntegration.payments"
        IQKeyboardManager.shared.enable = true
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if url.scheme?.localizedCaseInsensitiveCompare("com.tridhya.PaymentIntegration.payments") == .orderedSame {
            return BTAppContextSwitcher.sharedInstance.handleOpen(url)
        }
        
        return false
    }
}

