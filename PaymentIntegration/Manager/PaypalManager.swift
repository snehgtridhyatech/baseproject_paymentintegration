//
//  PaypalManager.swift
//  PaymentIntegration
//
//  Created by Sneh on 10/11/23.
//

import Foundation
import Braintree

class PaypalManager {
    
    // MARK: - Properties
    
    /// Shared instance of  class
    static let shared = PaypalManager()
    
    /// Set auth key according to live mode and sand box
    fileprivate var authKey = ""
    
    /// Paypal api client
    fileprivate var braintreeClient: BTAPIClient!
    
    /// Paypal vault request
    fileprivate var vaultRequest = BTPayPalVaultRequest()
    
    /// Paypal client
    fileprivate var payPalClient: BTPayPalClient!
    
    
    // MARK: - Public functions
    
    /// Use this function to generate account nonce
    func getAccountNonce(Authkey authKey: String,_ completion:@escaping (_ isSuccess: Bool,_ response: String) -> Void) {
        
        self.braintreeClient = BTAPIClient(authorization: authKey)
        self.payPalClient = BTPayPalClient(apiClient: self.braintreeClient)
        
        payPalClient.tokenize(self.vaultRequest) { (tokenizedPayPalAccount, error) in
            if let error = error {
                completion(false,error.localizedDescription)
            } else {
                if let account = tokenizedPayPalAccount {
                    completion(true,account.nonce)
                }
            }
        }
    }
    
}
