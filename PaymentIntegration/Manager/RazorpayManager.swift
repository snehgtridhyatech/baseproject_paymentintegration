//
//  RazorpayManager.swift
//  PaymentIntegration
//
//  Created by Sneh on 10/10/23.
//

import Foundation
import Razorpay
import CommonCrypto
import UIKit

/// Completion block to handle razorpay success and failure
fileprivate var razorpaySuccessBlock: ((Bool,Any) -> Void)!


class RazorpayManager {
    
    // MARK: - Properties
    
    /// Shared instance of  class
    static let shared = RazorpayManager()
    
    /// razorpay checkout
    fileprivate var razorPay: RazorpayCheckout?
    
    /// Set the test key for test mode and live key for live mode
    fileprivate var keyId = ""
    
    /// Set secret for create order
    fileprivate var secretKey = ""
    
    /// Set value for live & test mode
    fileprivate var isLive = true
    
    /// Set the orderId for live mode
    fileprivate var orderId = ""
    
    /// Set the controller from where you want to open razorpay
    fileprivate var displayController : UIViewController!
    
    
    // MARK: - Public function
    
    /// Use this function to setup and make payment with Razorpay
    func setup(KeyId keyId: String, SecretKey secretKey: String, Parent displayController: UIViewController, LiveMode isLive: Bool, OrderId orderId: String = "", RazorpayPaymentModel model: RazorpayPaymentModel, _ paymentSuccessBlock: @escaping ((_ isSuccess: Bool,_ response: Any) -> Void)) {
        
        self.keyId = keyId
        self.secretKey = secretKey
        self.displayController = displayController
        self.isLive = isLive
        
        if isLive && orderId != "" {
            self.orderId = orderId
        }
        
        self.razorPay = RazorpayCheckout.initWithKey(keyId, andDelegateWithData: self.displayController)
        
        razorpaySuccessBlock = { (isSuccess,response) in
            paymentSuccessBlock(isSuccess,response)
        }
        
        self.makePayment(model: model)
    }
    
    
    
    // MARK: - Private functions
    
    /// Use this function to make payment with Razorpay
    fileprivate func makePayment(model:RazorpayPaymentModel) {
        if model.amount.trimmingCharacters(in: .whitespacesAndNewlines) != "", let doubleAmount = Double(model.amount) {
            if self.checkKey() {
                if isLive {
                    if orderId != "" {
                        self.openRazorpayController(orderId: orderId,amount:Int(doubleAmount),model: model)
                    } else {
                        completion(status: .failure,message: "OrderId is required for Live mode")
                    }
                } else {
                    self.createOrderApi(model: model, amount: Int(doubleAmount))
                }
            } else {
                completion(status: .failure,message: "Valid KeyId & SecretKey is required to make payment with Razorpay")
            }
        } else {
            completion(status: .failure,message: "Please enter valid amount")
        }
    }
    
    
    // MARK: - Create order
    
    /// This function is used to create order based on amount details in razorpay (Note : KeyId and SecretKey is needed to use this function)
    fileprivate func createOrderApi(model: RazorpayPaymentModel,amount: Int) {
        if let url = URL(string: "https://api.razorpay.com/v1/orders") {
            let parameters: [String: Any] = [
                "amount": "\(amount * 100)",
                "currency": "INR",
                "receipt": "receipt_1"
            ]
            var request = URLRequest(url: url)
            request.httpMethod = "post"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
            
            let authString = "\(self.keyId):\(self.secretKey)"
            let authData = authString.data(using: String.Encoding.utf8)!
            let authKey = authData.base64EncodedString()
            request.setValue("Basic \(authKey)", forHTTPHeaderField: "Authorization")
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        self.completion(status: .failure,message: error?.localizedDescription ?? "")
                    } else {
                        if let data = data {
                            do {
                                let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                                if let errorResponse = jsonResponse?["error"] as? [String:Any] {
                                    let errorDesc = errorResponse["description"] as? String
                                    self.completion(status: .failure,message: errorDesc ?? "Something went wrong")
                                } else {
                                    if let orderId = jsonResponse?["id"] as? String {
                                        self.openRazorpayController(orderId: orderId,amount:amount,model: model)
                                    } else {
                                        self.completion(status: .failure,message: "Something went wrong")
                                    }
                                }
                            } catch(let err) {
                                self.completion(status: .failure,message: err.localizedDescription)
                            }
                        }
                    }
                }
            }.resume()
        }
    }
    
    
    // MARK: - Open razorpay page
    
    /// This function is used to open Razorpay page (Note : amount is required in parmeters)
    fileprivate func openRazorpayController(orderId:String,amount: Int,model:RazorpayPaymentModel) {
        let options: [String:Any] = [
            "amount": "\(amount * 100)",
            "currency": "INR",
            "description": model.description,
            "order_id": "\(orderId)",
            "image": model.imageUrl,
            "name": model.name,
            "prefill": [
                "contact": model.prefillName,
                "email": model.prefillEmail
            ],
            "theme": [
                "color": model.themeColor
            ]
        ]
        
        DispatchQueue.main.async {
            self.razorPay?.open(options,displayController: self.displayController)
        }
    }
    
    
    // MARK: - Verify signature
    
    /// This function is used to verify payment signature after payment completion
    fileprivate func verifyRazorpaySignature(razorpayOrderId:String,razorpayPaymentId:String,razorpaySignature:String) {
        let dataToSign = "\(razorpayOrderId)|\(razorpayPaymentId)"
        if let signData = dataToSign.data(using: .utf8), let secretData = self.secretKey.data(using: .utf8) {
            var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
            signData.withUnsafeBytes { dataBytes in
                secretData.withUnsafeBytes { keyBytes in
                    CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), keyBytes.baseAddress, keyBytes.count, dataBytes.baseAddress, dataBytes.count, &digest)
                }
            }
            let hmacData = Data(digest)
            let signFromIds =  hmacData.map { String(format: "%02hhx", $0) }.joined()
            self.razorPay?.close()
            
            if signFromIds == razorpaySignature {
                completion(status: .success)
            } else {
                completion(status: .failure,message: "Payment verifiaction failed.\nPlease contact your admin.")
            }
            
        }
    }
    
    fileprivate func checkKey() -> Bool {
        return self.keyId != "" && self.secretKey != ""
    }
    
    fileprivate func completion(status: RazorpayCompletionEnum,message: Any = "") {
        if razorpaySuccessBlock != nil {
            razorpaySuccessBlock(status == .success, message)
        }
    }
    
}

// MARK: - Enum
fileprivate enum RazorpayCompletionEnum {
    case success
    case failure
}


// MARK: - Extension

extension UIViewController : RazorpayPaymentCompletionProtocolWithData {
    public func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        RazorpayManager().completion(status: .failure,message: str)
    }
    
    public func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        let razorpayOrderId = response?["razorpay_order_id"] as! String
        let razorpayPaymentId = response?["razorpay_payment_id"] as! String
        let razorpaySignature = response?["razorpay_signature"] as! String
        if RazorpayManager.shared.isLive {
            RazorpayManager().completion(status: .success,message: ["razorpay_order_id":razorpayOrderId,
                                                                    "razorpay_payment_id":razorpayPaymentId,
                                                                    "razorpay_signature":razorpaySignature])
        } else {
            RazorpayManager.shared.verifyRazorpaySignature(razorpayOrderId: razorpayOrderId , razorpayPaymentId: razorpayPaymentId, razorpaySignature: razorpaySignature)
        }
    }
}



// MARK: - Model class
struct RazorpayPaymentModel {
    
    /// Amount you want to transfer
    var amount : String
    
    /// Image will show at the top left of razor pay page
    var imageUrl : String = ""
    
    /// Name will show at the top of razor pay page
    var name : String = ""
    
    /// Description will show in order details
    var description : String = ""
    
    /// Theme color of razor pay. Write has value of color.
    var themeColor : String = ""
    
    /// This will auto fill contact name in account details
    var prefillName : String = ""
    
    /// This will auto fill contact email in account details
    var prefillEmail : String = ""
}

