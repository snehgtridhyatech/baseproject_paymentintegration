//
//  StripeManager.swift
//  PaymentIntegration
//
//  Created by Sneh on 10/6/23.
//

import Foundation
import Stripe

class StripeManager {
    
    // MARK: - Properties
    
    /// Shared instance of  class
    static let shared = StripeManager()
    
    /// Set stripe publish key
    fileprivate var publishKey = ""
    
    /// Set stripe auth key for testing payment
    fileprivate var authKey = ""
    
    /// Specify the cards you want to include as supported options. By default, all card types are enabled.
    fileprivate var supportedCards : [STPCardBrand] = []
    
    /// Set value for live & test mode
    fileprivate var isLive = true
    
    
    // MARK: - Public functions
    
    /// Use this function to setup and make payment with Stripe
    func setup(StripeCardModel cardModel : StripeCardModel,StripePaymentModel paymentModel : StripePaymentModel = StripePaymentModel(amount: "", currency: .none, paymentDesc: ""),PublishKey publishKey: String, AuthKey authKey: String, Cards supportedCards: [STPCardBrand] = [], LiveMode isLive: Bool,_ completion : @escaping(_ isSuccess: Bool,_ response: String) -> Void) {
        
        StripeAPI.defaultPublishableKey = publishKey
        
        self.publishKey = publishKey
        self.authKey = authKey
        self.supportedCards = supportedCards
        self.isLive = isLive
        
        if isLive {
            self.getStripeToken(cardModel, completion)
        } else {
            if paymentModel.currency == .none {
                completion(false,"Valid StripePaymentModel is required in Sandbox mode")
            } else {
                self.getStripeTokenWithTestPayment(cardModel, paymentModel, completion)
            }
        }
    }
    
    
    // MARK: - Private functions
    
    /// Use this function to get stripe token to pass in server
    fileprivate func getStripeToken(_ model : StripeCardModel,_ completion : @escaping(Bool,String) -> Void) {
        if !self.checkKey(type: .publish) {
            completion(false,"PublishKey is required to generate stripe token")
        } else {
            let cardParams = STPCardParams()
            cardParams.number = model.cardNumber
            cardParams.expMonth = model.cardExpMonth
            cardParams.expYear = model.cardExpYear
            cardParams.cvc = model.cardCVV
            
            if self.isCardSupported(cardParams: cardParams) {
                self.generateStripeToken(cardParams: cardParams, completion)
            } else {
                completion(false,"Card is invalidate or not supported")
            }
        }
    }
    
    
    /// Use this function to generate stripe token with testing payment (Note: This will only work in US stripe admin account and Sandbox mode)
    fileprivate func getStripeTokenWithTestPayment(_ model : StripeCardModel,_ paymentModel : StripePaymentModel,_ completion : @escaping(Bool,String) -> Void) {
        if !self.checkKey(type: .both) {
            completion(false,"PublishKey & AuthKey are required to generate stripe token and test payment in Sandbox mode")
        } else {
            self.getStripeToken(model) { isSuccess, response in
                if isSuccess {
                    self.callStripePaymentApi(model: paymentModel, stripeToken: response, completion)
                } else {
                    completion(false,response)
                }
            }
        }
    }
    
    
    // MARK: - Generate Stripe token
    
    /// Use this function  to generate stripe token from default stripe method
    fileprivate func generateStripeToken(cardParams: STPCardParams,_ completion : @escaping(Bool,String) -> Void) {
        STPAPIClient.shared.createToken(withCard: cardParams) { (token, error) in
            if error != nil {
                completion(false,error?.localizedDescription ?? "")
            } else {
                if let tokenId = token?.tokenId {
                    print("STRIPE TOKEN : ",tokenId)
                    completion(true,tokenId)
                }
            }
        }
    }
    
    
    // MARK: - Stripe test payment api
    
    /// Use this function to test stripe payment with generated token
    fileprivate func callStripePaymentApi(model:StripePaymentModel,stripeToken:String,_ completion : @escaping(Bool,String) -> Void) {
        if let url = URL(string: "https://api.stripe.com/v1/charges") {
            var request = URLRequest(url: url)
            request.httpMethod = "post"
            let parameters : Data = "amount=\(model.amount)&currency=\(model.currency.rawValue)&description=\(model.paymentDesc)&source=\(stripeToken)".data(using: .utf8)!
            request.setValue("Bearer \(self.authKey)", forHTTPHeaderField: "Authorization")
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type");
            request.httpBody = parameters
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                    if error != nil {
                        completion(false,error?.localizedDescription ?? "")
                    } else {
                        if (data != nil && data?.count != 0 ) {
                            let response = String(data: data!, encoding: .utf8)
                            completion(true,response ?? "")
                        }
                    }
                }
            }.resume()
        }
    }
    
    
    /// This functions will check whether the card entered by user is included in suppoertedCards or not.
    fileprivate func isCardSupported(cardParams: STPCardParams) -> Bool {
        guard let number = cardParams.number else { return false }
        let cardBrand = STPCardValidator.brand(forNumber: number)
        if self.supportedCards.count > 0 {
            let filteredCard = self.supportedCards.filter({ brand in
                brand == cardBrand
            })
            if filteredCard.count > 0 {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
    
    /// This functions will check that required keys are available or not.
    fileprivate func checkKey(type: StripeKeyType) -> Bool {
        switch type {
        case .both:
            return self.authKey != "" && self.publishKey != ""
        case .auth:
            return self.authKey != ""
        case .publish:
            return self.publishKey != ""
        }
    }
}


// MARK: - Structs

/// This model class includes card details for validate card and generate token
struct StripeCardModel {
    let cardNumber : String
    let cardExpMonth : UInt
    let cardExpYear : UInt
    let cardCVV : String
}

/// This model class includes payment details for test payment
struct StripePaymentModel {
    let amount : String
    let currency : Currency
    let paymentDesc : String
}


// MARK: - Enums

fileprivate enum StripeKeyType {
    case both
    case auth
    case publish
}

enum Currency : String {
    case none = "NONE"
    case USD = "USD"
    case AED = "AED"
    case AFN = "AFN"
    case ALL = "ALL"
    case AMD = "AMD"
    case ANG = "ANG"
    case AOA = "AOA"
    case ARS = "ARS"
    case AUD = "AUD"
    case AWG = "AWG"
    case AZN = "AZN"
    case BAM = "BAM"
    case BBD = "BBD"
    case BDT = "BDT"
    case BGN = "BGN"
    case BIF = "BIF"
    case BMD = "BMD"
    case BND = "BND"
    case BOB = "BOB"
    case BRL = "BRL"
    case BSD = "BSD"
    case BWP = "BWP"
    case BYN = "BYN"
    case BZD = "BZD"
    case CAD = "CAD"
    case CDF = "CDF"
    case CHF = "CHF"
    case CLP = "CLP"
    case CNY = "CNY"
    case COP = "COP"
    case CRC = "CRC"
    case CVE = "CVE"
    case CZK = "CZK"
    case DJF = "DJF"
    case DKK = "DKK"
    case DOP = "DOP"
    case DZD = "DZD"
    case EGP = "EGP"
    case ETB = "ETB"
    case EUR = "EUR"
    case FJD = "FJD"
    case FKP = "FKP"
    case GBP = "GBP"
    case GEL = "GEL"
    case GIP = "GIP"
    case GMD = "GMD"
    case GNF = "GNF"
    case GTQ = "GTQ"
    case GYD = "GYD"
    case HKD = "HKD"
    case HNL = "HNL"
    case HTG = "HTG"
    case HUF = "HUF"
    case IDR = "IDR"
    case ILS = "ILS"
    case INR = "INR"
    case ISK = "ISK"
    case JMD = "JMD"
    case JPY = "JPY"
    case KES = "KES"
    case KGS = "KGS"
    case KHR = "KHR"
    case KMF = "KMF"
    case KRW = "KRW"
    case KYD = "KYD"
    case KZT = "KZT"
    case LAK = "LAK"
    case LBP = "LBP"
    case LKR = "LKR"
    case LRD = "LRD"
    case LSL = "LSL"
    case MAD = "MAD"
    case MDL = "MDL"
    case MGA = "MGA"
    case MKD = "MKD"
    case MMK = "MMK"
    case MNT = "MNT"
    case MOP = "MOP"
    case MUR = "MUR"
    case MVR = "MVR"
    case MWK = "MWK"
    case MXN = "MXN"
    case MYR = "MYR"
    case MZN = "MZN"
    case NAD = "NAD"
    case NGN = "NGN"
    case NIO = "NIO"
    case NOK = "NOK"
    case NPR = "NPR"
    case NZD = "NZD"
    case PAB = "PAB"
    case PEN = "PEN"
    case PGK = "PGK"
    case PHP = "PHP"
    case PKR = "PKR"
    case PLN = "PLN"
    case PYG = "PYG"
    case QAR = "QAR"
    case RON = "RON"
    case RSD = "RSD"
    case RUB = "RUB"
    case RWF = "RWF"
    case SAR = "SAR"
    case SBD = "SBD"
    case SCR = "SCR"
    case SEK = "SEK"
    case SGD = "SGD"
    case SHP = "SHP"
    case SLE = "SLE"
    case SOS = "SOS"
    case SRD = "SRD"
    case STD = "STD"
    case SZL = "SZL"
    case THB = "THB"
    case TJS = "TJS"
    case TOP = "TOP"
    case TRY = "TRY"
    case TTD = "TTD"
    case TWD = "TWD"
    case TZS = "TZS"
    case UAH = "UAH"
    case UGX = "UGX"
    case UYU = "UYU"
    case UZS = "UZS"
    case VND = "VND"
    case VUV = "VUV"
    case WST = "WST"
    case XAF = "XAF"
    case XCD = "XCD"
    case XOF = "XOF"
    case XPF = "XPF"
    case YER = "YER"
    case ZAR = "ZAR"
    case ZMW = "ZMW"
}
